import { Bar } from 'react-chartjs-2';
import zoomPlugin from 'chartjs-plugin-zoom';

const BarChart = ({ data, reverseX = false }: any) => {
  return (
    <div>
      <Bar
        data={data}
        height={'500px'}
        redraw={true}
        options={{
          plugins: {
            zoom: {
              pan: {
                enabled: true,
                mode: 'x',
              },
              zoom: {
                pinch: {
                  enabled: true, // Enable pinch zooming
                },
                wheel: {
                  enabled: true, // Enable wheel zooming
                },
                mode: 'x',
              },
            },
          },
          maintainAspectRatio: false,
          scales: {
            x: {
              reverse: reverseX,
            },
            y: {
              beginAtZero: true,
            },
          },
        }}
      />
    </div>
  );
};

export default BarChart;
