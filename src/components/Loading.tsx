import { Spin } from 'antd';

const Loading = () => {
  return (
    <div className='flex flex-col justify-center p-5'>
      <Spin size='large' />
    </div>
  );
};

export default Loading;
