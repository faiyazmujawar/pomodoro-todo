import { AiTwotoneHeart } from 'react-icons/ai';

const Footer = () => {
  return (
    <footer>
      <div className='w-full text-center py-1'>
        <p>
          Made with <AiTwotoneHeart size={'20px'} color='blue' /> by Faiyaz
        </p>
      </div>
    </footer>
  );
};

export default Footer;
