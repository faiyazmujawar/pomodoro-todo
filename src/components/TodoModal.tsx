import useQuery from '@/hooks/useQuery';
import { useEffect, useState } from 'react';
import {
  deleteTodoMutation,
  getTodoById,
  markTodoAsDone,
} from './ApolloQueries';
import { Todo } from '@prisma/client';
import Loading from './Loading';
import { Modal, Popconfirm, Tag } from 'antd';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { IoCheckmarkDoneOutline } from 'react-icons/io5';
import {
  getFormattedDate,
  getTodoStatusColor,
  getTodoStatusText,
} from '@/utils/TodoUtils';
import useMutation from '@/hooks/useMutation';
import { useTodos } from '@/context/TodoContext';

interface TodoModalProps {
  open: boolean;
  onClose: () => unknown;
  onClickUpdate: () => unknown;
  todoId: string;
}

const TodoModal = ({
  todoId,
  open,
  onClose,
  onClickUpdate,
}: TodoModalProps) => {
  const { todos, setTodos } = useTodos();

  const [todo, setTodo] = useState<Todo | undefined>(undefined);
  const {
    operation: getTodo,
    result: { loading },
  } = useQuery<{ getTodoById: Todo }>(getTodoById, {
    onCompleted: ({ getTodoById }) => {
      setTodo(getTodoById);
    },
  });
  const { operation: markDone } = useMutation(markTodoAsDone, {
    onCompleted: () => {
      onClose();
    },
  });
  const {
    operation: deleteTodo,
    result: { loading: deleting },
  } = useMutation<{ deleteTodo: boolean }>(deleteTodoMutation, {
    onCompleted: () => {
      setTodos(todos.filter((t) => t.id !== todoId));
      onClose();
    },
  });

  useEffect(() => {
    if (open) {
      getTodo({ id: todoId });
    }
  }, [open]);

  if (loading !== false) {
    return (
      <Modal open={open} onCancel={onClose} centered>
        <Loading />
      </Modal>
    );
  }

  return (
    <Modal
      open={open}
      title={todo!.title}
      footer={null}
      onCancel={onClose}
      centered
      destroyOnClose
    >
      <div className='tags mb-2'>
        <Tag>
          {todo!.isComplete
            ? `Completed on ${getFormattedDate(todo!.completedAt!)}`
            : `Due on ${getFormattedDate(todo!.dueDate)}`}
        </Tag>
        <Tag color={getTodoStatusColor(todo!)}>{getTodoStatusText(todo!)}</Tag>
      </div>
      <p>{todo!.description}</p>
      <div className='actions grid grid-column-3 grid-flow-col gap-2 mt-2'>
        <div
          className='rounded-md py-2 hover:bg-green-50'
          onClick={() => markDone({ id: todoId })}
        >
          <IoCheckmarkDoneOutline size={'20px'} />
        </div>
        <div
          className='rounded-md py-2 hover:bg-yellow-50'
          onClick={onClickUpdate}
        >
          <AiOutlineEdit size={'20px'} />
        </div>
        <Popconfirm
          title={'Delete this todo?'}
          onConfirm={() => {
            deleteTodo({ id: todoId });
          }}
          okButtonProps={{ loading: deleting }}
          okText={'Yes'}
          cancelText={'No'}
        >
          <div className='rounded-md py-2 hover:bg-red-50'>
            <AiOutlineDelete size={'20px'} />
          </div>
        </Popconfirm>
      </div>
    </Modal>
  );
};

export default TodoModal;
