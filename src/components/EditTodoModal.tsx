import { TodoInput } from '@/graphql/resolvers/types';
import useMutation from '@/hooks/useMutation';
import { Todo } from '@prisma/client';
import { Button, DatePicker, Input, Modal, notification } from 'antd';
import dayjs from 'dayjs';
import { ChangeEvent, useState } from 'react';
import { createTodoMutation, updateTodoMutation } from './ApolloQueries';
import { useTodos } from '@/context/TodoContext';
import TextArea from 'antd/lib/input/TextArea';
import _ from 'lodash';

interface EditTodoModalProps {
  mode: 'EDIT' | 'CREATE';
  todo?: Todo;
  open: boolean;
  onClose: () => unknown;
}

const EditTodoModal = ({ mode, todo, open, onClose }: EditTodoModalProps) => {
  const { todos, setTodos } = useTodos();
  const [todoInput, setTodoInput] = useState<TodoInput>({
    title: todo?.title ?? '',
    description: todo?.description ?? '',
    dueDate: todo?.dueDate
      ? dayjs(todo.dueDate).toDate()
      : dayjs().add(1, 'day').toDate(),
  });

  function handleChange(
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    const { name, value } = event.target;
    setTodoInput({
      ...todoInput,
      [name]: value,
    });
  }

  const {
    operation: createTodo,
    result: { loading: creating },
  } = useMutation<{ createTodo: Todo }>(createTodoMutation, {
    onCompleted: ({ createTodo: newTodo }) => {
      setTodos([newTodo, ...todos]);
      notification.success({ message: 'Task added successfully!' });
      onClose();
    },
  });

  const {
    operation: updateTodo,
    result: { loading: updating },
  } = useMutation<{ updateTodo: Todo }>(updateTodoMutation, {
    onCompleted: () => {
      notification.success({ message: 'Task updated successfully' });
      onClose();
    },
  });

  return (
    <Modal
      open={open}
      destroyOnClose={true}
      title={mode === 'CREATE' ? 'Add Task' : 'Edit task'}
      onCancel={onClose}
      footer={[
        <Button
          key='submit'
          type='primary'
          loading={creating || updating}
          onClick={() => {
            switch (mode) {
              case 'CREATE':
                createTodo({ todo: todoInput });
                break;
              case 'EDIT':
                updateTodo({ id: todo!.id, update: todoInput });
                break;
            }
          }}
        >
          {mode === 'CREATE' ? 'Add' : 'Save'}
        </Button>,
      ]}
    >
      <div className='flex mb-2'>
        <Input
          name='title'
          required
          value={todoInput.title}
          onChange={handleChange}
          className='w-2/3 mr-2'
          placeholder='Title...'
        />
        <DatePicker
          value={dayjs(todoInput.dueDate)}
          onChange={(e) => {
            if (!_.isNil(e)) {
              setTodoInput({
                ...todoInput,
                dueDate: e.toDate(),
              });
            }
          }}
        />
      </div>
      <TextArea
        rows={6}
        placeholder='Description...'
        name='description'
        value={todoInput.description}
        onChange={handleChange}
      />
    </Modal>
  );
};

export default EditTodoModal;
