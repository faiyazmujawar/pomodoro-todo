'use client';

import useQuery from '@/hooks/useQuery';
import { getAllTodosQuery, getTodosByText } from './ApolloQueries';
import { useTodos } from '@/context/TodoContext';
import { useEffect, useState } from 'react';
import _, { debounce, isEmpty } from 'lodash';
import { Todo } from '@prisma/client';
import Loading from './Loading';
import TodoCard from './TodoCard';
import { Button, Input } from 'antd';
import { BiSearch } from 'react-icons/bi';
import EditTodoModal from './EditTodoModal';

const TodoList = () => {
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [searching, setSearching] = useState(false);
  const [searchText, setSearchText] = useState('');

  const searchTodos = debounce((text) => {
    setSearching(true);
    getTodosByTextOperation({ text });
  }, 1000);

  const { todos, setTodos } = useTodos();
  const {
    operation: getTodos,
    result: { loading },
  } = useQuery<{ getAllTodos: Todo[] }>(getAllTodosQuery, {
    onCompleted: ({ getAllTodos }) => {
      setTodos(getAllTodos);
    },
  });
  const { operation: getTodosByTextOperation } = useQuery<{
    getTodosByText: Todo[];
  }>(getTodosByText, {
    onCompleted: ({ getTodosByText: data }) => {
      setTodos(data);
      setSearching(false);
    },
  });

  useEffect(() => {
    getTodos();
  });

  useEffect(() => {
    if (isEmpty(searchText)) {
      getTodos();
    } else {
      searchTodos(searchText);
    }
  }, [searchText]);

  if (loading !== false || searching === true) {
    return <Loading />;
  }

  return (
    <div className='w-4/5 m-auto mb-4'>
      <div className='search-bar sticky top-0 z-10 p-6 rounded'>
        <div className='w-1/3  m-auto flex justify-between'>
          <div className='w-full mr-2'>
            <Input
              size='large'
              value={searchText}
              addonBefore={<BiSearch size={'20px'} />}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              placeholder='Search todos...'
            />
          </div>
          <Button
            size='large'
            type='primary'
            color='#e6f9af'
            onClick={() => setShowCreateModal(true)}
          >
            Add New
          </Button>
          <EditTodoModal
            mode='CREATE'
            open={showCreateModal}
            onClose={() => setShowCreateModal(false)}
          />
        </div>
      </div>
      <div className='todo-list mb-4 px-2'>
        {todos.map((todo) => (
          <TodoCard key={todo.id} todo={todo} />
        ))}
      </div>
    </div>
  );
};

export default TodoList;
