import { Doughnut } from 'react-chartjs-2';
import 'chart.js/auto';

const DoughnutChart = ({ data }: any) => {
  return (
    <div>
      <Doughnut
        data={data}
        redraw={true}
        options={{
          radius: '80%',
          maintainAspectRatio: false,
        }}
      />
    </div>
  );
};

export default DoughnutChart;
