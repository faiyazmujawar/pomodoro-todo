import { getFormattedDate, getTodoStatusColor } from '@/utils/TodoUtils';
import { Todo } from '@prisma/client';
import TodoModal from './TodoModal';
import { useState } from 'react';
import EditTodoModal from './EditTodoModal';

// FIXME: todo details modal is not closing on clicking close button

interface TodoCardProps {
  todo: Todo;
}

const TodoCard = ({ todo }: TodoCardProps) => {
  const [openTodoModal, setOpenTodoModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  return (
    <div
      className={`border border-solid border-gray-300 rounded-md border-l-4 p-3 flex flex-col justify-between cursor-pointer hover:shadow-md`}
      style={{ borderLeftColor: getTodoStatusColor(todo) }}
      onClick={() => setOpenTodoModal(true)}
    >
      <h5 className='mb-2'>{todo.title}</h5>
      <p className='text-xs text-gray-400 italic'>
        Due on {getFormattedDate(todo.dueDate)}
      </p>
      <TodoModal
        open={openTodoModal}
        onClose={() => {
          setOpenTodoModal(false);
          console.log('false');
        }}
        onClickUpdate={() => {
          setOpenTodoModal(false);
          setShowEditModal(true);
        }}
        todoId={todo.id}
      />
      <EditTodoModal
        open={showEditModal}
        mode='EDIT'
        todo={todo}
        onClose={() => setShowEditModal(false)}
      />
    </div>
  );
};

export default TodoCard;
