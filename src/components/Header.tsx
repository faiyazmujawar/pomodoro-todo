import { useUser } from '@auth0/nextjs-auth0/client';
import { Button, Dropdown } from 'antd';
import type { MenuProps } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Header = () => {
  const router = useRouter();
  const { isLoading, user, error } = useUser();

  if (isLoading === false && user === undefined) {
    return (
      <div>
        <a href='/api/auth/logout'>
          <Button>Logout</Button>
        </a>
      </div>
    );
  }

  const menuItems: MenuProps['items'] = [
    {
      key: 1,
      label: <a href='/api/auth/logout'>Logout</a>,
    },
  ];

  return (
    <div className='py-8 px-10 flex justify-between bg-blue-500 text-white'>
      <div className='text-4xl'>TasKeeper</div>
      <div className='flex items-center justify-center'>
        <div className='mr-8'>
          <Link
            href={router.pathname.endsWith('dashboard') ? '/' : '/dashboard'}
            passHref
          >
            <Button size='large'>
              {router.pathname.endsWith('dashboard') ? 'Home' : 'Dashboard'}
            </Button>
          </Link>
        </div>
        {isLoading === false && (
          <div className='flex'>
            <div className='mr-4 rounded-full flex flex-col justify-center'>
              <img
                src={user?.picture ?? ''}
                className='rounded-full'
                height={'50px'}
                alt='profile-img'
              />
            </div>
            <Dropdown
              trigger={['click']}
              className='cursor-pointer'
              menu={{ items: menuItems }}
            >
              <div className='flex flex-col justify-center'>
                <h2>{user?.nickname}</h2>
                <p className='text-sm'>{user?.email}</p>
              </div>
            </Dropdown>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
