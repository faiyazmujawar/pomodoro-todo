import { Todo } from '@prisma/client';
import dayjs from 'dayjs';

export function getTodoStatusColor({ isComplete, dueDate }: Todo) {
  if (isComplete) return 'green';
  const dueOn = dayjs(dueDate);
  const today = dayjs();
  if (today.isAfter(dueOn)) return 'red';
  if (dueOn.diff(today, 'days') < 7) return 'orange';
  return '#dee2e6';
}

export function getTodoStatusText({ isComplete, dueDate }: Todo) {
  if (isComplete) return 'Completed';
  const dueOn = dayjs(dueDate);
  const today = dayjs();
  if (today.isAfter(dueOn)) return 'Overdue';
  if (dueOn.diff(today, 'days') < 7) return 'Due shortly';
  return 'Pending';
}

export function getFormattedDate(date: Date) {
  return dayjs(date).format('D MMM, YYYY');
}
