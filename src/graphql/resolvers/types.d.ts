export interface TodoInput {
  title: string;
  description: string;
  dueDate: Date;
}

export interface TodoUpdate {
  title?: string;
  description?: string;
  dueDate?: Date;
}

export interface Stats {
  total: number;
  completed: number;
  overdue: number;
  pending: number;
  byMonth: any;
}
