import { Context } from '@/pages/api/graphql';
import { prisma } from '@lib/PrismaClient';
import dayjs from 'dayjs';

export async function getStats(_parent: any, _args: any, { user }: Context) {
  const todos = await prisma.todo.findMany({
    where: { userEmail: user.email },
  });
  let total = todos.length;
  let completed = 0;
  let pending = 0;
  let overdue = 0;
  const byMonth: any = {};
  todos.forEach((todo) => {
    const createdOn = dayjs(new Date(todo.createdAt));
    const dueOn = dayjs(new Date(todo.dueDate));
    const today = dayjs();
    const createdOnMonth = createdOn.format('MMM, YYYY');
    const completedOn = todo.completedAt
      ? dayjs(new Date(todo.completedAt))
      : null;
    const completedOnMonth = completedOn?.format('MMM, YYYY');

    const createdMonth = byMonth[createdOnMonth] ?? {
      created: 0,
      completed: 0,
    };
    createdMonth.created += 1;

    if (todo.isComplete) {
      completed += 1;
      const completedMonth = byMonth[completedOnMonth!] ?? {
        created: 0,
        completed: 0,
      };
      completedMonth.completed += 1;
      byMonth[completedOnMonth!] = completedMonth;
    } else {
      if (today.isAfter(dueOn)) overdue += 1;
      pending += 1;
    }

    byMonth[createdOnMonth] = createdMonth;
  });

  return {
    total,
    completed,
    pending,
    overdue,
    byMonth,
  };
}
