import * as TodoQueries from './TodoQuery';
import * as StatsQueries from './Statistics';

export default {
  ...TodoQueries,
  ...StatsQueries,
};
