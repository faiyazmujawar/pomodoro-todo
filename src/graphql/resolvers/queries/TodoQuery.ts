import { Context } from '@/pages/api/graphql';
import { prisma } from '@lib/PrismaClient';
import { Todo } from '@prisma/client';

export async function getAllTodos(
  _parent: any,
  _args: any,
  { user }: Context
): Promise<Todo[]> {
  return await prisma.todo.findMany({
    where: { userEmail: user.email },
  });
}

export async function getTodosByText(
  _parent: any,
  { text }: { text: string },
  { user }: Context
) {
  const regex = text.split(/\s+/).join('|');
  return await prisma.todo.findMany({
    where: { AND: [{ title: { search: regex } }, { userEmail: user.email }] },
  });
}

export async function getTodoById(
  _parent: any,
  { id }: { id: string },
  { user }: Context
) {
  try {
    return await prisma.todo.findUnique({
      where: { id, AND: [{ userEmail: user.email }] },
    });
  } catch (error: any) {
    if (error.code === 'P2025') {
      console.error('Not found');
    }
    return null;
  }
}
