import { GraphQLJSON, GraphQLDateTime } from 'graphql-scalars';
import mutations from './mutation';
import queries from './queries';

export default {
  DateTime: GraphQLDateTime,
  JSON: GraphQLJSON,
  Query: {
    sayHi: () => 'Hello',
    ...queries,
  },
  Mutation: {
    ...mutations,
  },
};
