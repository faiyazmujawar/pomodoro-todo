import { Todo } from '@prisma/client';
import { TodoInput, TodoUpdate } from '../types';
import { prisma } from '@lib/PrismaClient';
import { Context } from '@/pages/api/graphql';
import _ from 'lodash';

export async function createTodo(
  _parent: any,
  { todo }: { todo: TodoInput },
  { user }: Context
): Promise<Todo> {
  return await prisma.todo.create({
    data: {
      ...todo,
      userEmail: user.email,
    },
  });
}

export async function deleteTodo(
  _parent: any,
  { id }: { id: string },
  { user }: Context
): Promise<Todo> {
  const todo = await prisma.todo.delete({
    where: {
      id,
      AND: [{ userEmail: user.email }],
    },
  });

  return todo;
}

export async function updateTodo(
  _parent: any,
  {
    id,
    update: { title, description, dueDate },
  }: { id: string; update: TodoUpdate },
  { user }: Context
): Promise<Todo> {
  try {
    const todo = await prisma.todo.findFirst({
      where: { id, AND: [{ userEmail: user.email }] },
    });
    if (todo === null || todo === undefined) {
      throw new Error('Not found');
    }
    todo.title = title ?? todo.title;
    todo.description = description ?? todo.description;
    todo.dueDate = dueDate ?? todo.dueDate;

    return await prisma.todo.update({ where: { id }, data: todo });
  } catch (error) {
    throw new Error('Not found');
  }
}

export async function markAsDone(
  _parent: any,
  { id }: { id: string },
  { user }: Context
) {
  return (
    (await prisma.todo.update({
      where: { id, AND: [{ userEmail: user.email }] },
      data: { isComplete: true, completedAt: new Date() },
    })) !== undefined
  );
}
