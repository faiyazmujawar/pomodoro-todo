export default `
  # Scalars
  scalar DateTime
  scalar JSON

  type Query {
    sayHi: String!
    getAllTodos: [Todo]!
    getTodoById(id: ID!): Todo!
    getTodosByText(text: String!): [Todo]!
    getStats: Stats!
  }

  type Mutation {
    createTodo(todo: TodoInput!): Todo!
    updateTodo(id: ID!, update: TodoUpdate!): Todo!
    deleteTodo(id: ID!): Boolean!
    markAsDone(id: ID!): Boolean!
  }

  # Todo

  input TodoInput {
    title: String!
    description: String!
    dueDate: DateTime!
  }

  input TodoUpdate {
    title: String
    description: String
    dueDate: DateTime
  }

  type Todo {
    id: ID!
    title: String!
    description: String!
    isComplete: Boolean!
    dueDate: DateTime!
    createdAt: DateTime!
    updatedAt: DateTime!
    completedAt: DateTime
  }

  # Dashboard
  type Stats {
    total: Int!
    completed: Int!
    overdue: Int!
    pending: Int!
    byMonth: JSON!
  }
`;
