import typeDefs from '@/graphql/types';
import resolvers from '@/graphql/resolvers';

import {
  YogaInitialContext,
  createGraphQLError,
  createSchema,
  createYoga,
} from 'graphql-yoga';
import { IncomingMessage, ServerResponse } from 'http';
import { Claims, getSession } from '@auth0/nextjs-auth0';
import _ from 'lodash';

export interface Context extends YogaInitialContext {
  req: IncomingMessage;
  res: ServerResponse;
  user: Claims;
}

const schema = createSchema<Context>({
  typeDefs,
  resolvers,
});

export const config = {
  api: { bodyParser: false },
};

export default createYoga<{ req: IncomingMessage; res: ServerResponse }>({
  schema,
  context: async ({ req, res }) => {
    const session = await getSession(req, res);
    if (_.isNil(session)) {
      throw createGraphQLError('User must be logged in', {
        extensions: { code: 'FORBIDDEN' },
      });
    }
    return { user: session?.user };
  },
  graphqlEndpoint: '/api/graphql',
});
