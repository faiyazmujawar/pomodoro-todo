import { useEffect, useState } from 'react';
import 'chart.js/auto';
import { Divider } from 'antd';
import DoughnutChart from '@/components/DoughnutChart';
import BarChart from '@/components/BarChart';
import useQuery from '@/hooks/useQuery';
import { getStats } from '@/components/ApolloQueries';
import { Stats } from '@/graphql/resolvers/types';
import Loading from '@/components/Loading';

const Dashboard = () => {
  const [overviewData, setOverviewData] = useState<any>(undefined);
  const [byMonthData, setByMonthData] = useState<any>(undefined);
  const [totalCount, setTotalCount] = useState(0);

  const {
    operation: getStatsOperation,
    result: { loading },
  } = useQuery<{ getStats: Stats }>(getStats, {
    onCompleted: ({ getStats: stats }) => {
      setTotalCount(stats.total);
      setOverviewData({
        labels: ['Done', 'Pending', 'Overdue'],
        datasets: [
          {
            data: [stats.completed, stats.pending, stats.overdue],
            backgroundColor: [
              'rgba(75, 192, 192, 0.5)',
              'rgba(201, 203, 207, 0.5)',
              'rgba(255, 99, 132, 0.5)',
            ],
            borderColor: [
              'rgba(75, 192, 192)',
              'rgba(201, 203, 207)',
              'rgba(255, 99, 132)',
            ],
            borderWidth: 1,
          },
        ],
      });
      setByMonthData({
        labels: Object.keys(stats.byMonth),
        datasets: [
          {
            label: 'Created',
            data: Object.values(stats.byMonth)
              .map((value: any) => value.created)
              .reverse(),
            backgroundColor: ['rgba(54, 162, 235, 0.2)'],
            borderColor: ['rgb(54, 162, 235)'],
            borderWidth: 1,
          },
          {
            label: 'Completed',
            data: Object.values(stats.byMonth)
              .map((value: any) => value.completed)
              .reverse(),
            backgroundColor: ['rgba(75, 192, 192, 0.2)'],
            borderColor: ['rgb(75, 192, 192)'],
            borderWidth: 1,
          },
        ],
      });
    },
  });

  useEffect(() => {
    getStatsOperation();
  }, []);

  return (
    <div>
      {loading !== false ? (
        <div className='flex flex-col justify-center h-screen'>
          <Loading />
        </div>
      ) : (
        <div className='w-4/5 m-auto mt-4'>
          <div className='overview'>
            <h1 className='mb-2'>Overview</h1>
            <p className='text-sm text-gray-400 italic '>
              Total {totalCount} tasks shown
            </p>
            <div className='chart flex justify-center'>
              <DoughnutChart data={overviewData} />
            </div>
          </div>
          <Divider className='my-12' />
          <div className='by-month mb-20'>
            <h1 className='mb-4'>Segregation by month</h1>
            <BarChart reverseX={true} data={byMonthData} />
          </div>
        </div>
      )}
    </div>
  );
};

export default Dashboard;
