import TodoList from '@/components/TodoList';
import TodoContextProvider from '@/context/TodoContext';

export default function Home() {
  return (
    <TodoContextProvider>
      <div className='relative'>
        <div className='todo-grid'>
          <TodoList />
        </div>
      </div>
    </TodoContextProvider>
  );
}
