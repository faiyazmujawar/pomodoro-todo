import '@/styles/globals.css';
import type { AppProps } from 'next/app';
import { UserProvider } from '@auth0/nextjs-auth0/client';
import Header from '@/components/Header';
import Footer from '@/components/Footer';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <UserProvider>
      <div className='h-screen'>
        <div className='sticky top-0 z-10'>
          <Header />
        </div>
        <Component {...pageProps} />
        <Footer />
      </div>
    </UserProvider>
  );
}
