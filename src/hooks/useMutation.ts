import axios from 'axios';
import { useState } from 'react';

interface UseMutationOptions<T> {
  onCompleted?: (data: T) => void;
  onError?: (error: any) => void;
}

export default function useMutation<T>(
  mutation: string,
  { onCompleted, onError }: UseMutationOptions<T> = {}
) {
  const [loading, setLoading] = useState<boolean | undefined>(false);
  const [data, setData] = useState<T | undefined>(undefined);

  async function operation(variables: any) {
    setLoading(true);
    const response = await axios.post(
      process.env.GRAPHQL_URI ?? '/api/graphql',
      { query: mutation, variables },
      { headers: { 'Content-Type': 'application/json' } }
    );

    if (response.status === 200) {
      setData(response.data.data);
      onCompleted?.(response.data.data);
    } else {
      onError?.(response.data.errors);
    }
    setLoading(false);
  }

  return { operation, result: { loading, data } };
}
